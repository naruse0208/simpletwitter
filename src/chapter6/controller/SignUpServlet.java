package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

//URL(signup)を参照
@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	//GETの場合はsignup.jspを表示
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		/*表示させたいJSPのパスを引数に指定し、RequestDispatcherオブジェクトを取得、
		  forward()メソッドをコールすることでJSPに処理を移す*/
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	//POSTの場合は、リクエストパラメータをUserオブジェクトにセットし、Serviceのメソッドを呼び出してDBへユーザーの登録を行い
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		//JSPで入力された(登録したい)ユーザーの情報(id,name,account…全て入ってる)
		User user = getUser(request);

		//入力値に対するバリデーション(入力値が不正な場合は自画面(signup)を表示させる)
		if (!isValid(user, errorMessages)) {
			//JSPで表示させるためのメッセージ(オブジェクト)をHttpServletRequestオブジェクトにセット
			request.setAttribute("errorMessages", errorMessages);
			/*表示させたいJSPのパスを引数に指定し、RequestDispatcherオブジェクトを取得、
			  forward()メソッドをコールすることでJSPに処理を移す*/
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);

		//sendRedirectにすることで、ユーザーの登録が完了したら再びトップ画面を表示する(PRGパターン)
		response.sendRedirect("./");
	}


	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setEmail(request.getParameter("email"));
		user.setDescription(request.getParameter("description"));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String email = user.getEmail();

		if(!StringUtils.isEmpty(name) && (20 < name.length())) {
			errorMessages.add("名前は20文字以下で入力してください");
		}

		if(StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		}
		else if(20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}

		if(StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if(!StringUtils.isEmpty(email) && (50 < email.length())) {
			errorMessages.add("メールアドレスは50文字以下で入力してください");
		}

		User preUser = new UserService().select(user.getAccount());

		if(preUser != null) {
			errorMessages.add("アカウント名は重複しています");
			return false;
		}

		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}